import createTypography from '@material-ui/core/styles/createTypography';

import breakpoints from './breakpoints';
import palette from './palette';

export default createTypography(palette, {
  fontFamily: 'Montserrat',
  h1: {
    fontWeight: 500,
    fontSize: 32,
    lineHeight: '44px',
    letterSpacing: '0.1px',
    [breakpoints.up('lg')]: {
      fontSize: 48,
      lineHeight: '56px',
      letterSpacing: 0,
    },
  },
  h2: {
    fontWeight: 500,
    fontSize: 28,
    lineHeight: '38px',
    letterSpacing: '0.1px',
    [breakpoints.up('lg')]: {
      fontSize: 36,
      lineHeight: '48px',
      letterSpacing: 0,
    },
  },
  h3: {
    fontWeight: 500,
    fontSize: 24,
    lineHeight: '32px',
    letterSpacing: '0.1px',
    [breakpoints.up('lg')]: {
      fontSize: 32,
      lineHeight: '44px',
    },
  },
  h4: {
    fontWeight: 500,
    fontSize: 20,
    lineHeight: '28px',
    letterSpacing: '-0.3px',
    [breakpoints.up('lg')]: {
      fontSize: 24,
      lineHeight: '32px',
      letterSpacing: '0.1px',
    },
  },
  h5: {
    fontWeight: 500,
    fontSize: 16,
    lineHeight: '24px',
    letterSpacing: '-0.5px',
    [breakpoints.up('lg')]: {
      fontSize: 20,
      lineHeight: '28px',
      letterSpacing: '-0.2px',
    },
  },
  subtitle1: {
    fontSize: 14,
    lineHeight: '20px',
    fontWeight: 600,
    [breakpoints.up('lg')]: {
      fontSize: 16,
      lineHeight: '24px',
    },
  },
  subtitle2: {
    fontSize: 14,
    lineHeight: '20px',
    fontWeight: 500,
  },
  body1: {
    fontSize: 14,
    lineHeight: '20px',
    fontWeight: 400,
    [breakpoints.up('lg')]: {
      fontSize: 16,
      lineHeight: '24px',
    },
  },
  body2: {
    fontSize: 14,
    lineHeight: '20px',
    fontWeight: 400,
  },
  button: {
    fontSize: 14,
    lineHeight: '16px',
    textTransform: 'uppercase',
    fontWeight: 500,
    letterSpacing: '0.75px',
  },
  overline: {
    fontSize: 12,
    lineHeight: '16px',
    fontWeight: 500,
    textTransform: 'uppercase',
  },
  caption: {
    fontSize: 12,
    lineHeight: '16px',
    fontWeight: 400,
  },
});
