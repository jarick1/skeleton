import createPalette from '@material-ui/core/styles/createPalette';

export default createPalette({
  primary: {
    main: '#165EFE',
  },
  secondary: {
    main: '#2ECC71',
  },
  error: {
    main: '#EF4056',
  },
  text: {
    primary: '#020F2D',
    secondary: '#6D7885',
    hint: '#165EFE',
  },
});
