import { createMuiTheme } from '@material-ui/core/styles';

import breakpoints from './breakpoints';
import spacing from './spacing';
import palette from './palette';
import typography from './typography';

const theme = createMuiTheme({
  breakpoints,
  palette,
  typography,
  spacing,
  overrides: {
    MuiContainer: {
      maxWidthLg: {
        paddingLeft: spacing(5),
        paddingRight: spacing(5),
        [breakpoints.up('lg')]: {
          paddingLeft: 0,
          paddingRight: 0,
          maxWidth: 1136,
        },
        [breakpoints.up('xl')]: {
          maxWidth: 1232,
        },
      },
      maxWidthMd: {
        width: '100%',
        paddingLeft: spacing(5),
        paddingRight: spacing(5),
        maxWidth: 896,
        [breakpoints.up('md')]: {
          maxWidth: 896,
        },
      },
    },
    MuiButton: {
      startIcon: {
        width: 16,
        height: 16,
        '& svg': {
          maxWidth: '100%',
          maxHeight: '100%',
        },
      },
      sizeLarge: {
        padding: spacing(4),
        borderRadius: 8,
        fontSize: 14,
      },
    },
    MuiInput: {
      input: {
        fontSize: 16,
        lineHeight: '24px',
        color: '#011136',
        caretColor: '#165EFE',
        '&::placeholder': {
          color: '#6D7885',
          opacity: 1,
        },
      },
      underline: {
        '&:before, &:after': {
          display: 'none',
        },
      },
      root: {
        height: 24,
      },
    },
    MuiFilledInput: {
      root: {
        border: '1px solid rgba(1, 17, 54, 0.12)',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        borderBottomLeftRadius: 8,
        borderBottomRightRadius: 8,
        backgroundColor: '#f7f7f7',
        '&:hover': {
          backgroundColor: '#f0f2f866',
        },
        '&$focused': {
          borderColor: palette.primary.main,
          backgroundColor: '#F7F7F766',
        },
        '&$error': {
          backgroundColor: 'rgba(239, 64, 86, 0.03)',
          borderColor: '#EF4056',
        },
      },
      underline: {
        '&:before, &:after': {
          display: 'none',
        },
      },
    },
    MuiCheckbox: {
      root: {
        padding: spacing(3),
      },
    },
    MuiSelect: {
      select: {
        border: '0.5px solid rgba(1, 17, 54, 0.12)',
        borderRadius: 8,
        paddingLeft: spacing(3),
        height: 48,
        backgroundColor: '#ffffff',
        '&:focus': {
          borderRadius: 8,
          backgroundColor: '#ffffff',
        },
      },
      icon: {
        right: spacing(3),
      },
    },
    MuiTooltip: {
      tooltip: {
        backgroundColor: '#011136',
        fontSize: 14,
        lineHeight: '20px',
        letterSpacing: '0.15px',
        textAlign: 'center',
      },
      arrow: {
        color: '#011136',
      },
    },
    MuiExpansionPanel: {
      root: {
        borderTop: '1px solid #D8DADE',
        background: '#FBFBFB',
        boxShadow: 'none',
        '&.Mui-expanded': {
          margin: 0,
        },
        '&:before': {
          opacity: 0,
        },
      },
    },
    MuiExpansionPanelSummary: {
      root: {
        marginLeft: 'auto',
        marginRight: 'auto',
        paddingLeft: spacing(5),
        paddingRight: spacing(5),
        [breakpoints.up('lg')]: {
          paddingLeft: 0,
          paddingRight: 0,
          maxWidth: 1136,
        },
        [breakpoints.up('xl')]: {
          maxWidth: 1232,
        },
      },
      content: {
        marginTop: 0,
        marginBottom: 0,
        '&.Mui-expanded': {
          margin: 0,
        },
      },
    },
    MuiGridListTile: {
      tile: {
        overflow: 'visible',
      },
    },
  },
  shadows: [
    'none',
    '0px 1px 2px rgba(1, 17, 54, 0.24), 0px 1px 2px rgba(1, 17, 54, 0.12), 0px 0px 1px rgba(1, 17, 54, 0.16)',
    '0px 1px 8px rgba(1, 17, 54, 0.1), 0px 2px 4px rgba(1, 17, 54, 0.12), 0px 1px 2px rgba(1, 17, 54, 0.16)',
    '0px 4px 4px rgba(1, 17, 54, 0.08), 0px 0px 4px rgba(1, 17, 54, 0.06)',
    '0px 1px 12px rgba(1, 17, 54, 0.12), 0px 4px 8px rgba(1, 17, 54, 0.02), 0px 1px 4px rgba(1, 17, 54, 0.01)',
    '0px 1px 12px rgba(1, 17, 54, 0.04), 0px 4px 8px rgba(1, 17, 54, 0.08), 0px 3px 6px rgba(1, 17, 54, 0.1)',
    '0px 1px 16px rgba(1, 17, 54, 0.12), 0px 6px 12px rgba(1, 17, 54, 0.12), 0px 4px 8px rgba(1, 17, 54, 0.1)',
    '0px 1px 24px rgba(1, 17, 54, 0.12), 0px 8px 16px rgba(1, 17, 54, 0.12), 0px 6px 12px rgba(1, 17, 54, 0.12)',
    '0px 2px 16px rgba(1, 17, 54, 0.12), 0px 1px 12px rgba(1, 17, 54, 0.07)',
    '0px 0px 52px rgba(1, 17, 54, 0.16), 0px 0px 28px rgba(1, 17, 54, 0.16), 0px 0px 18px rgba(1, 17, 54, 0.16)',
    '0px 1px 32px rgba(1, 17, 54, 0.12), 0px 12px 24px rgba(1, 17, 54, 0.12), 0px 8px 16px rgba(1, 17, 54, 0.12)',
    '0px 1px 48px rgba(1, 17, 54, 0.12), 0px 16px 32px rgba(1, 17, 54, 0.12), 0px 12px 24px rgba(1, 17, 54, 0.12)',
  ],
});

export default theme;
