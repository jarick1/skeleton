import React, { useCallback } from 'react';
import PropTypes from 'prop-types';

import ModalContent from './elements/ModalContent';

const Modal = ({ openModalHandler, isModalOpen }) => {
  const handleLogout = useCallback(() => {
    openModalHandler();
  }, [openModalHandler]);

  return <ModalContent handleLogout={handleLogout} openModalHandler={openModalHandler} isModalOpen={isModalOpen} />;
};

Modal.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  openModalHandler: PropTypes.func.isRequired,
};

export default Modal;
