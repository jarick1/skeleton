import React, { memo } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Box, Button, Dialog, DialogActions, DialogTitle } from '@material-ui/core';

import useStyles from './styled';

import CloseIcon from '../assets/close.svg';

const ModalContent = ({ isModalOpen, openModalHandler, handleLogout }) => {
  const classes = useStyles();
  return (
    <Dialog open={isModalOpen} onClose={openModalHandler} className={classes.modal} maxWidth="md">
      <Box className={classes.modal}>
        <CloseIcon onClick={openModalHandler} className={classes.closeIcon} fill="#8995AD" />
        <DialogTitle className={classes.heading} disableTypography>
          Уверены, что хотите выйти?
        </DialogTitle>
        <DialogActions className={classes.buttons}>
          <Button autoFocus onClick={openModalHandler} variant="contained" color="primary" className={classes.button}>
            Остаться
          </Button>
          <Button onClick={handleLogout} color="primary" className={clsx(classes.button, classes.buttonExit)}>
            Выйти
          </Button>
        </DialogActions>
      </Box>
    </Dialog>
  );
};

ModalContent.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  openModalHandler: PropTypes.func.isRequired,
  handleLogout: PropTypes.func.isRequired,
};

export default memo(ModalContent);
