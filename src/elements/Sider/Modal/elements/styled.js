import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  modal: {
    padding: '60px 80px 40px',
    borderRadius: '12px',
  },

  heading: {
    textAlign: 'center',
    marginBottom: '50px',
    fontSize: '42px',
    fontWeight: '600',
    lineHeight: 1.5,
  },

  closeIcon: {
    position: 'absolute',
    right: '16px',
    top: '16px',
    cursor: 'pointer',
  },
  buttons: {
    display: 'flex',
    width: '95%',
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 'auto',
  },

  button: {
    width: '200px',
    height: '40px',
    justifyContent: 'center',
    textAlign: 'center',
  },

  buttonExit: {
    backgroundColor: 'transparent',
    border: 'none',
    boxShadow: '0 0',

    '&:hover': {
      boxShadow: '0 0',
      backgroundColor: 'transparent',
    },
  },
}));

export default useStyles;
