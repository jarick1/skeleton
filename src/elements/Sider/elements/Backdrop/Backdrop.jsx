import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Box } from '@material-ui/core';

import useStyles from './styled';

const Backdrop = ({ isSidebarOpen }) => {
  const classes = useStyles();

  return <Box className={clsx(classes.backdrop, { [classes.animation]: isSidebarOpen })} />;
};

Backdrop.defaultProps = {
  isSidebarOpen: false,
};

Backdrop.propTypes = {
  isSidebarOpen: PropTypes.bool,
};

export default Backdrop;
