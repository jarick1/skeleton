import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  backdrop: {
    width: '100vw',
    height: '100vh',
    backgroundColor: 'rgba(1, 17, 54, 0.4)',
    backdropFilter: 'blur(10px)',
    position: 'fixed',
    top: '0',
    left: '0',
    opacity: '0',
    zIndex: '-1',
    transition: 'opacity .4s ease, z-index .4s ease',
  },
  animation: {
    opacity: '1',
    zIndex: '999',
  },
}));

export default useStyles;
