import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    paddingTop: '32px',
    paddingBottom: '20px',
    overflowY: 'auto',
    overflowX: 'hidden',
    justifyContent: 'space-between',
  },

  list: {
    listStyleType: 'none',
    paddingLeft: '0',
    marginBottom: '10px',

    [theme.breakpoints.down('sm')]: {
      marginBottom: '20px',
    },
  },
}));

export default useStyles;
