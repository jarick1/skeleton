import React from 'react';
import PropTypes from 'prop-types';

import LogoutBlock from '../LogoutBlock/LogoutBlock';
import ScoreList from '../../ScoreList/ScoreList';
import NavigationItemWrapper from '../../NavigationItemWrapper/NavigationItemWrapper';
import SubscriptionUpdate from '../../SubscriptionUpdate/SubscriptionUpdate';
import useStyles from './styled';

const NavigationContent = ({ items, isSidebarOpen, openModalHandler, openSidebarHandler }) => {
  const classes = useStyles();

  return (
    <nav className={classes.container}>
      <ul className={classes.list}>
        {items.map((item, index) => (
          <NavigationItemWrapper
            key={index}
            isSidebarOpen={isSidebarOpen}
            {...item}
            openSidebarHandler={openSidebarHandler}
          />
        ))}
        <ScoreList key="list" isSidebarOpen={isSidebarOpen} />
      </ul>
      <SubscriptionUpdate isSidebarOpen={isSidebarOpen} />
      <LogoutBlock isSidebarOpen={isSidebarOpen} openModalHandler={openModalHandler} />
    </nav>
  );
};

NavigationContent.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.func.isRequired,
      to: PropTypes.string,
      as: PropTypes.string,
      isRoot: PropTypes.bool,
      isMobile: PropTypes.bool,
    }),
  ).isRequired,
  isSidebarOpen: PropTypes.bool.isRequired,
  openModalHandler: PropTypes.func.isRequired,
  openSidebarHandler: PropTypes.func.isRequired,
};

export default NavigationContent;
