import React, { memo } from 'react';
import PropTypes from 'prop-types';

import useStyles from './styled';

const MenuLink = ({ to, children, target }) => {
  const classes = useStyles();

  return (
    <a href={to} target={target} className={classes.link}>
      {children}
    </a>
  );
};

MenuLink.defaultProps = {
  icon: () => {},
  target: '_self',
};

MenuLink.propTypes = {
  to: PropTypes.string.isRequired,
  children: PropTypes.oneOfType([PropTypes.object, PropTypes.array]).isRequired,
  icon: PropTypes.func,
  target: PropTypes.string,
};

export default memo(MenuLink);
