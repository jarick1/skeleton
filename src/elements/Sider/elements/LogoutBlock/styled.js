import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  link: {
    display: 'flex',
    listStyle: 'none',
    padding: '13px 22px',
    overflow: 'hidden',
    transition: 'background 0.3s',
    cursor: 'pointer',
    marginTop: '-20px',
    minHeight: '47px',
    backgroundColor: 'transparent',
    boxShadow: 'none',
    justifyContent: 'flex-start',
    width: '256px',
    startIcon: '',

    '&:hover': {
      boxShadow: 'none',
      background: '#f2f6ff',
      borderRadius: '8px',
    },
  },
  title: {
    textAlign: 'left',
    marginLeft: '19px',
    lineHeight: '1.6',
    fontWeight: '500',
    whiteSpace: 'nowrap',
    opacity: '0',
    transition: 'opacity .4s ease',
    textTransform: 'capitalize',
    letterSpacing: '0',
  },
  animation: {
    opacity: '1',
  },
}));

export default useStyles;
