import React, { memo } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Box, Button } from '@material-ui/core';

import useStyles from './styled';

import LogoutIcon from '../../assets/LogoutIcon.svg';

const LogoutBlock = ({ isSidebarOpen, openModalHandler }) => {
  const classes = useStyles();

  return (
    <Button variant="contained" className={classes.link} startIcon={<LogoutIcon />} onClick={openModalHandler}>
      <Box component="span" className={clsx(classes.title, { [classes.animation]: isSidebarOpen })}>
        Выйти
      </Box>
    </Button>
  );
};

LogoutBlock.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,
  openModalHandler: PropTypes.func.isRequired,
};

export default memo(LogoutBlock);
