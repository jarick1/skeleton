import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    listStyle: 'none',
    transition: 'background 0.3s',
    display: 'flex',
    alignItems: 'center',
    position: 'relative',

    '&:nth-child(1)': {
      display: 'none',
    },

    '&:nth-child(4)': {
      marginBottom: '16px',
      '&::before': {
        overflow: 'hidden',
        transition: 'width 0.3s ease',
        content: '""',
        position: 'absolute',
        bottom: '-16px',
        left: '0',
        width: '100%',
        height: '1px',
        margin: '8px 0',
        background: 'rgba(1, 17, 54, 0.12)',
      },
    },
    '&:nth-child(6)': {
      marginBottom: '16px',
      '&::before': {
        overflow: 'hidden',
        transition: 'width 0.3s ease',
        content: '""',
        position: 'absolute',
        bottom: '-16px',
        left: '0',
        width: '100%',
        height: '1px',
        margin: '8px 0',
        background: 'rgba(1, 17, 54, 0.12)',
      },
    },

    '&:hover': {
      borderRadius: '8px',
      background: '#f2f6ff',
    },

    [theme.breakpoints.down('sm')]: {
      display: 'none',
      '&:nth-child(1)': {
        display: 'flex',
      },
    },
  },

  iconContainer: {
    display: 'flex',
    flexShrink: '0',
    width: '22px',
    height: '22px',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '0',
  },

  icon: {
    '& path': {
      fill: '#939eab',
    },
    '& circle': {
      stroke: '#939eab',
    },
    '& rect': {
      fill: '#939eab',
    },
  },
  active: {
    '& path': {
      fill: '#165efe',
    },
    '& circle': {
      stroke: '#165efe',
    },
    '& rect': {
      fill: '#165efe',
    },
    '& link': {
      color: '#165efe',
    },
  },

  link: {
    display: 'flex',
    width: '100%',
    height: '100%',
    padding: '13px 17px',
    cursor: 'pointer',
  },

  title: {
    marginLeft: '22px',
    lineHeight: '1.6',
    whiteSpace: 'nowrap',
    fontWeight: '500',
    fontSize: '14px',
    opacity: '0',
    transition: 'opacity .4s ease',
  },
  animation: {
    opacity: '1',
  },

  showOnMobileUpBar: {
    display: 'none',

    [theme.breakpoints.down('sm')]: {
      display: 'flex',
    },
  },
  mobile: {
    [theme.breakpoints.down('sm')]: {
      display: 'flex',
    },
  },
}));

export default useStyles;
