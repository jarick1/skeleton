import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Box, Typography, IconButton } from '@material-ui/core';

import MenuLink from '../MenuLink/MenuLink';
import useStyles from './styled';

const NavigationItem = ({
  to,
  target,
  isLinkActive,
  icon: Icon,
  noLink,
  isMobile,
  isUpBar,
  isSidebarOpen,
  title,
  openSidebarHandler,
}) => {
  const classes = useStyles();

  const iconButton = (
    <IconButton className={clsx(classes.iconContainer, { [classes.link]: noLink })}>
      <Icon className={isLinkActive ? classes.active : classes.icon} />
    </IconButton>
  );

  return (
    <li
      className={clsx(classes.container, {
        [classes.showOnMobileUpBar]: isUpBar,
        [classes.mobile]: isMobile,
        [classes.active]: isLinkActive,
      })}
    >
      <Box onClick={openSidebarHandler}>
        {!noLink ? (
          <MenuLink to={to} target={target}>
            {iconButton}
            {!isUpBar ? (
              <Typography className={clsx(classes.title, { [classes.animation]: isSidebarOpen })}>{title}</Typography>
            ) : null}
          </MenuLink>
        ) : (
          iconButton
        )}
      </Box>
    </li>
  );
};
NavigationItem.defaultProps = {
  to: '/',
  isLinkActive: false,
  target: '_self',
  isSidebarOpen: false,
  title: '',
  isUpBar: false,
  isMobile: false,
  noLink: false,
  openSidebarHandler: () => {},
};

NavigationItem.propTypes = {
  icon: PropTypes.func.isRequired,
  to: PropTypes.string,
  isLinkActive: PropTypes.bool,
  target: PropTypes.string,
  isSidebarOpen: PropTypes.bool,
  title: PropTypes.string,
  openSidebarHandler: PropTypes.func,
  isUpBar: PropTypes.bool,
  isMobile: PropTypes.bool,
  noLink: PropTypes.bool,
};

export default NavigationItem;
