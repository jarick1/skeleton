import React, { memo } from 'react';
import PropTypes from 'prop-types';

import SubscriptionContent from './elements/SubscriptionContent';

const SubscriptionUpdate = ({ isSidebarOpen }) => {
  return (
    <SubscriptionContent
      href="/subscriptions"
      isSidebarOpen={isSidebarOpen}
      shouldVisibleSubscription={false}
      subscriptionTypeMessage=""
      daysNumber={-1}
      daysPlural=""
      date={null}
    />
  );
};

SubscriptionUpdate.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,
};

export default memo(SubscriptionUpdate);
