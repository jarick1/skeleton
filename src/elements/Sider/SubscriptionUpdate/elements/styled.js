import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  subscription: {
    width: '100%',
    minWidth: '256px',
    opacity: '0',
    transition: 'opacity .4s ease',
    height: '100%',

    '& a': {
      cursor: 'pointer',
    },

    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },

  background: {
    margin: '0 auto',
    display: 'block',
  },

  content: {
    backgroundColor: '#165efe',
    borderRadius: '12px',
    padding: '13px 0 10px 13px',
    display: 'flex',
    top: '-20px',
    margin: '0 auto',
    position: 'relative',
    width: '85%',
  },

  rightSide: {
    paddingLeft: '10px',
    paddingRight: '20px',
    color: 'white',
  },

  title: {
    fontSize: '12px',
    lineHeight: '16px',
    color: '#ffffff',
    fontWeight: '500',
    marginBottom: '3px',
  },
  text: {
    fontSize: '12px',
    lineHeight: '16px',
    color: '#ffffff',
    display: 'block',
  },
  animation: {
    opacity: '1',
  },
}));

export default useStyles;
