import React, { memo } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Link, Box } from '@material-ui/core';

import useStyles from './styled';

import SubscriptionImage from '../assets/subscriptionImage.svg';
import SubscriptionInnerIcon from '../assets/subscriptionInnerIcon.svg';

const SubscriptionContent = ({
  isSidebarOpen,
  shouldVisibleSubscription,
  href,
  subscriptionTypeMessage,
  date,
  daysNumber,
  daysPlural,
}) => {
  const classes = useStyles();

  return (
    <Box className={clsx(classes.subscription, { [classes.animation]: isSidebarOpen && shouldVisibleSubscription })}>
      <Link href={href}>
        <SubscriptionImage className={classes.background} />
        <Box className={classes.content}>
          <Box className={classes.leftSide}>
            <SubscriptionInnerIcon className={classes.innerIcon} />
          </Box>
          <Box className={classes.rightSide}>
            <h3 className={classes.title}>Продлите свою {subscriptionTypeMessage} подписку</h3>
            <p className={classes.text}>{date ? `Истекает через ${daysNumber} ${daysPlural}` : null} </p>
          </Box>
        </Box>
      </Link>
    </Box>
  );
};

SubscriptionContent.defaultProps = {
  date: Date.now(),
};

SubscriptionContent.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,
  shouldVisibleSubscription: PropTypes.bool.isRequired,
  href: PropTypes.string.isRequired,
  subscriptionTypeMessage: PropTypes.string.isRequired,
  date: PropTypes.number,
  daysNumber: PropTypes.number.isRequired,
  daysPlural: PropTypes.string.isRequired,
};

export default memo(SubscriptionContent);
