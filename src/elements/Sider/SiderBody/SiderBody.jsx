import React, { memo } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Paper, Box } from '@material-ui/core';
import navigationConfig from '../navigationConfig';
import NavigationContent from '../elements/NavigationContent/NavigationContent';
import useStyles from './styled';

const SiderBody = ({ isSidebarOpen, openModalHandler, openSidebarHandler }) => {
  const classes = useStyles();

  return (
    <Paper elevation={3} className={clsx(classes.container, { [classes.containerOpened]: isSidebarOpen })}>
      <Box className={classes.inner}>
        <NavigationContent
          items={navigationConfig}
          isSidebarOpen={isSidebarOpen}
          openModalHandler={openModalHandler}
          openSidebarHandler={openSidebarHandler}
        />
      </Box>
    </Paper>
  );
};

SiderBody.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,
  openModalHandler: PropTypes.func.isRequired,
  openSidebarHandler: PropTypes.func.isRequired,
};

export default memo(SiderBody);
