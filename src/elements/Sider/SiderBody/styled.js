import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'relative',
    zIndex: '5',
    height: '100%',
    width: '100%',
    paddingTop: '56px',
    transition: 'width 0.3s ease',

    [theme.breakpoints.down('sm')]: {
      right: '-70px',
      width: '256px',
      transition: 'right 0.3s ease',
    },
  },

  containerOpened: {
    width: '256px',
  },

  [theme.breakpoints.down('sm')]: {
    containerOpened: {
      right: '256px',
    },
  },

  logoContainer: {
    zIndex: '5',
    position: 'absolute',
    top: '0',
    right: '0',
    width: '100%',
    height: '88px',
    background: ' #165efe',
    display: 'flex',
    justifyContent: 'flex-end',
    alignItems: 'center',
    paddingRight: '35px',

    '& svg': {
      width: '112px',
    },
  },

  expandIcon: {
    zIndex: '10',
    position: 'absolute',
    top: '0',
    width: '40px',
    height: '88px',
    left: '100%',
    border: 'none',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    background: '#165efe',
  },

  inner: {
    boxShadow:
      '0px 1px 4px rgba(1, 17, 54, 0.01), 0px 4px 8px rgba(1, 17, 54, 0.02), 0px 1px 12px rgba(1, 17, 54, 0.12)',
    background: '#ffffff',
    width: '100%',
    height: '100%',
  },
}));

export default useStyles;
