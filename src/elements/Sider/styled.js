import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    zIndex: '1000',
    position: 'fixed',
    right: 'calc(100% - 56px)',
    top: '0',
    width: '56px',
    height: '100vh',
  },

  [theme.breakpoints.down('sm')]: {
    container: {
      right: '-70px',
    },
  },
}));

export default useStyles;
