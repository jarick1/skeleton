import React, { memo } from 'react';
import PropTypes from 'prop-types';

import ScoreItem from './elements/ScoreItem';

import PointsIcon from './assets/points.svg';
import RatingIcon from './assets/rating.svg';

const items = [
  { title: 'Баллы', score: 120, icon: PointsIcon },
  { title: 'Рейтинг', score: 1200, icon: RatingIcon },
];

const ScoreList = ({ isSidebarOpen }) => {
  return items.map((item, index) => <ScoreItem key={index} isSidebarOpen={isSidebarOpen} {...item} />);
};

ScoreList.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,
};

export default memo(ScoreList);
