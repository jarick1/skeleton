import React, { memo } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Box } from '@material-ui/core';

import useStyles from './styled';

const ScoreItem = ({ icon: Icon, title, score, isSidebarOpen }) => {
  const classes = useStyles();

  return (
    <li className={classes.wrapper}>
      <Box className={classes.iconContainer}>
        <Box component="span" className={clsx(classes.smallScore, { [classes.animation]: !isSidebarOpen })}>
          {score}
        </Box>
        <Icon className={classes.icon} />
      </Box>
      <Box component="span" className={clsx(classes.title, { [classes.animation]: isSidebarOpen })}>
        {title}: {score}
      </Box>
    </li>
  );
};

ScoreItem.propTypes = {
  isSidebarOpen: PropTypes.bool.isRequired,
  icon: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  score: PropTypes.number.isRequired,
};

export default memo(ScoreItem);
