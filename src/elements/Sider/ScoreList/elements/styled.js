import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    listStyle: 'none',
    overflow: 'hidden',
    transition: 'background 0.3s',

    '&:hover': {
      borderRadius: '8px',
      background: '#f2f6ff',
    },
  },

  wrapper: {
    display: 'flex',
    width: '100%',
    padding: '9px 12px',

    '&:last-child': {
      '& div ': {
        backgroundImage: 'linear-gradient(to right, #165efe 0%, #5184f6 100%)',
      },
    },
  },
  [theme.breakpoints.down('sm')]: {
    wrapper: {
      display: 'flex',
      width: '100%',
      padding: '9px 12px',

      '&:nth-child(8)': {
        '& div ': {
          backgroundImage: 'linear-gradient(to right, #165efe 0%, #5184f6 100%)',
        },
      },
    },
  },

  iconContainer: {
    display: 'flex',
    flexShrink: '0',
    width: '33px',
    height: '32px',
    justifyContent: 'center',
    alignItems: 'center',
    padding: '6px',
    borderRadius: '10px',
    backgroundImage: 'linear-gradient(to right, #3fa9f5 0%, #7dd6ea 100%)',
    position: 'relative',
  },

  icon: {
    width: 'auto',
    height: 'auto',
  },

  title: {
    marginLeft: '16px',
    lineHeight: '32px',
    whiteSpace: 'nowrap',
    fontWeight: '500',
    opacity: '0',
    transition: 'opacity .4s ease',
  },

  smallScore: {
    position: 'absolute',
    top: '-10px',
    height: '16px',
    fontSize: '10px',
    fontWeight: '600',
    color: '#020f2d',
    padding: ' 3px 5px',
    lineHeight: '10px',
    backgroundColor: '#ffffff',
    boxShadow:
      '0px 3px 6px rgba(1, 17, 54, 0.1), 0px 4px 8px rgba(1, 17, 54, 0.08), 0px 1px 12px rgba(1, 17, 54, 0.04)',
    borderRadius: '99px',
    opacity: '0',
    transition: 'opacity .4s ease',
  },

  animation: {
    opacity: '1',
  },
}));

export default useStyles;
