import React, { useState, useCallback } from 'react';

import { ThemeProvider } from '@material-ui/core/styles';

import theme from '../../theme';
import SiderBody from './SiderBody/SiderBody';
import Backdrop from './elements/Backdrop/Backdrop';
import UpBar from './UpBar/UpBar';
import Modal from './Modal/Modal';

import useStyles from './styled';

const Sider = () => {
  const classes = useStyles();

  const [isSidebarOpen, setIsSidebarOpened] = useState(false);
  const [isModalOpen, setIsModalOpened] = useState(false);

  const openModalHandler = useCallback(() => setIsModalOpened(!isModalOpen), [isModalOpen, setIsModalOpened]);
  const openSidebarHandler = useCallback(() => setIsSidebarOpened(!isSidebarOpen), [isSidebarOpen, setIsSidebarOpened]);

  return (
    <ThemeProvider theme={theme}>
      <aside>
        <UpBar openSidebarHandler={openSidebarHandler} />
        <div
          onMouseOver={() => setIsSidebarOpened(true)}
          onFocus={() => setIsSidebarOpened(true)}
          onMouseLeave={() => setIsSidebarOpened(false)}
          onBlur={() => setIsSidebarOpened(false)}
          className={classes.container}
        >
          <SiderBody
            isSidebarOpen={isSidebarOpen}
            openSidebarHandler={openSidebarHandler}
            openModalHandler={openModalHandler}
          />
        </div>
        <Backdrop isSidebarOpen={isSidebarOpen} />
      </aside>
      {isModalOpen ? <Modal isModalOpen={isModalOpen} openModalHandler={openModalHandler} /> : null}
    </ThemeProvider>
  );
};

export default Sider;
