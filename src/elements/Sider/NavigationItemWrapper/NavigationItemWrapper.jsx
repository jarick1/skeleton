import React from 'react';
import PropTypes from 'prop-types';

import NavigationItem from '../elements/NavigationItem/NavigationItem';

const NavigationItemWrapper = ({
  to,
  target,
  icon,
  isSidebarOpen,
  isUpBar,
  isMobile,
  title,
  noLink,
  openSidebarHandler,
}) => {
  return (
    <NavigationItem
      to={to}
      target={target}
      isLinkActive={false}
      icon={icon}
      noLink={noLink}
      isMobile={isMobile}
      isUpBar={isUpBar}
      isSidebarOpen={isSidebarOpen}
      title={title}
      openSidebarHandler={openSidebarHandler}
    />
  );
};

NavigationItemWrapper.defaultProps = {
  to: '/',
  target: '_self',
  isRoot: false,
  isUpBar: false,
  isMobile: false,
  notColored: false,
  noLink: false,
};

NavigationItemWrapper.propTypes = {
  icon: PropTypes.func.isRequired,
  isSidebarOpen: PropTypes.bool.isRequired,
  title: PropTypes.string.isRequired,
  openSidebarHandler: PropTypes.func.isRequired,
  to: PropTypes.string,
  target: PropTypes.string,
  isRoot: PropTypes.bool,
  isUpBar: PropTypes.bool,
  isMobile: PropTypes.bool,
  notColored: PropTypes.bool,
  noLink: PropTypes.bool,
};

export default NavigationItemWrapper;
