import React, { useState, memo, useCallback } from 'react';
import PropTypes from 'prop-types';

import mobileNavigationConfigUpBar from './mobileNavigationConfigUpBar';
import UpBarContent from './elements/UpBarContent';

const UpBar = ({ openSidebarHandler }) => {
  const items = mobileNavigationConfigUpBar;
  const [isBurgerActive, setIsBurgerActive] = useState(false);

  const burgerToggleHandler = useCallback(() => {
    openSidebarHandler();
    setIsBurgerActive(!isBurgerActive);
  }, [openSidebarHandler, setIsBurgerActive, isBurgerActive]);

  return (
    <UpBarContent
      profileAvatar={null}
      items={items}
      burgerToggleHandler={burgerToggleHandler}
      isBurgerActive={isBurgerActive}
    />
  );
};

UpBar.propTypes = {
  openSidebarHandler: PropTypes.func.isRequired,
};

export default memo(UpBar);
