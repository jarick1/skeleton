import AchievementsIcon from '../assets/achievements.svg';
import LearningIcon from '../assets/learning.svg';
import NotificationIcon from './assets/notifications.svg';

export default [
  {
    icon: AchievementsIcon,
    to: '/achievements',
  },
  {
    icon: LearningIcon,
    to: '/',
  },
  {
    icon: NotificationIcon,
    noLink: true,
    notColored: true,
  },
];
