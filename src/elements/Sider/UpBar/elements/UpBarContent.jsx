import React, { memo } from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import { Paper, Box, Avatar } from '@material-ui/core';

import NavigationItem from '../../elements/NavigationItem';
import useStyles from './styled';

// import NotificationsIcon from '../assets/notifications.svg';
import Logo from '../../assets/logo.svg';

const UpBarContent = ({ items, isBurgerActive, burgerToggleHandler, profileAvatar }) => {
  const classes = useStyles();

  return (
    <Paper elevation={0} className={classes.upbar}>
      <Box className={classes.leftSide}>
        <Logo className={classes.logo} width="20px" height="32px" />
        <Box component="div" className={classes.wrapper}>
          {items.map((item, index) => {
            return <NavigationItem key={index} isUpBar {...item} />;
          })}
        </Box>
        <div
          className={clsx(classes.burger, classes.showOnMobile, { [classes.burgerActive]: isBurgerActive })}
          onClick={burgerToggleHandler}
          role="button"
        >
          <div className={classes.line} />
          <div className={classes.line} />
          <div className={classes.line} />
        </div>
      </Box>
      {/* <Box className={classes.rightSide}> 
        <Box className={classes.notifications}>
          <NotificationsIcon />
          <span className={classes.notificationsCount}>2</span>
        </Box> */}
      <Box className={classes.profile}>
        {profileAvatar ? <Avatar className={classes.profileAvatar} src={profileAvatar} /> : ''}
      </Box>
      {/* </Box> */}
    </Paper>
  );
};

UpBarContent.defaultProps = {
  profileAvatar: '',
};

UpBarContent.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      icon: PropTypes.func.isRequired,
      to: PropTypes.string,
      as: PropTypes.string,
      noLink: PropTypes.bool,
      notColored: PropTypes.bool,
    }),
  ).isRequired,
  isBurgerActive: PropTypes.bool.isRequired,
  burgerToggleHandler: PropTypes.func.isRequired,
  profileAvatar: PropTypes.string,
};

export default memo(UpBarContent);
