import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  upbar: {
    height: '56px',
    boxShadow: '0 1px 2px rgba(8, 35, 48, 0.24), 0 2px 6px rgba(8, 35, 48, 0.16)',
    position: 'fixed',
    top: '0',
    left: '0',
    width: '100%',
    zIndex: '1001',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: '0px 18px',
  },

  wrapper: {
    height: '100%',
    width: '70%',
    display: 'flex',
    justifyContent: 'space-around',
    alignItems: 'center',
  },

  [theme.breakpoints.down('sm')]: {
    upbar: {
      padding: '0px 22px',
    },
  },

  leftSide: {
    height: '100%',
    display: 'flex',
    alignItems: 'center',
  },

  [theme.breakpoints.down('sm')]: {
    leftSide: {
      width: '100%',
      justifyContent: 'space-between',
    },
  },

  rightSide: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '80px',

    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },

  notifications: {
    width: '32px',
    height: '32px',
    borderRadius: ' 50%',
    backgroundColor: '#165efe',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'relative',
    cursor: 'pointer',
  },

  notificationsCount: {
    backgroundColor: 'red',
    padding: '1px 4px 5px 5px',
    width: '16px',
    height: '16px',
    borderRadius: ' 50%',
    fontSize: '10px',
    fontWeight: '600',
    lineHeight: '16px',
    position: 'absolute',
    top: '-3px',
    right: '-3px',
    color: '#ffffff',
  },

  profile: {
    width: '32px',
    height: '32px',
    cursor: 'pointer',
  },

  profileAvatar: {
    borderRadius: '10px',
    width: '100%',
    height: '100%',
  },

  mobile: {
    display: 'none',
  },

  [theme.breakpoints.down('sm')]: {
    leftSide: {
      display: 'flex',
      width: '100%',
      justifyContent: 'space-between',
      alignItems: 'center',
    },

    profile: {
      marginRight: '50px',
    },
  },

  burger: {
    width: '20px',
    margin: 'auto 0',
  },

  line: {
    backgroundColor: '#939eab',
    borderRadius: '1px',
    height: '2px',
    margin: '4px 0',
    width: '100%',
  },

  showOnMobile: {
    display: 'none',

    [theme.breakpoints.down('sm')]: {
      display: 'block',
    },
  },

  burgerActive: {
    '& div': {
      '&:first-child': {
        transform: 'rotate(45deg) translate(5px, 5px)',
      },

      '&:nth-child(2)': {
        opacity: 0,
      },

      '&:last-child': {
        transform: 'rotate(-45deg) translate(3px, -4px)',
      },
    },
  },
}));

export default useStyles;
