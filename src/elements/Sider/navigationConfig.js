import AchievementsIcon from './assets/achievements.svg';
import CatalogIcon from './assets/catalog.svg';
import LearningIcon from './assets/learning.svg';
import SubscriptionIcon from './assets/subscription.svg';
import ProfileIcon from './assets/profile.svg';

export default [
  {
    title: 'Профиль',
    icon: ProfileIcon,
    to: '/profile',
    isMobile: true,
  },
  {
    title: 'Моё обучение',
    icon: LearningIcon,
    to: '/education',
  },
  {
    title: 'Достижения',
    icon: AchievementsIcon,
    to: '/achievements',
  },
  {
    title: 'Подписка',
    icon: SubscriptionIcon,
    to: '/subscriptions',
    isMobile: true,
  },
  {
    title: 'Каталог',
    icon: CatalogIcon,
    to: '/',
    isRoot: true,
    isMobile: true,
  },
];
