import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
  container: {
    paddingTop: 56,
    paddingLeft: 56,
    position: 'relative',

    [theme.breakpoints.down('sm')]: {
      paddingLeft: 0,
    },
  },
}));

export default useStyles;
