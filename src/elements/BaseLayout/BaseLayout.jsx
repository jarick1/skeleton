import React, { memo } from 'react';
import PropTypes from 'prop-types';

import { ThemeProvider } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';

import Sider from '../Sider';
import theme from '../../theme';
import useStyles from './styled';

const BaseLayout = ({ children, isShowSider }) => {
  const classes = useStyles();
  return (
    <ThemeProvider theme={theme}>
      {isShowSider && (
        <Box>
          <Sider />
        </Box>
      )}
      <Box className={isShowSider ? classes.container : ''}>{children}</Box>
    </ThemeProvider>
  );
};

BaseLayout.defaultProps = {
  isShowSider: true,
};

BaseLayout.propTypes = {
  isShowSider: PropTypes.bool,
};

export default memo(BaseLayout);
