import React from 'react';

import { Container } from '@material-ui/core';

const TestPage = () => <Container>Test page</Container>;

export default TestPage;
