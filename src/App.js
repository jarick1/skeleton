import React from 'react';

import TestPage from './components/test/TestPage';
import BaseLayout from './elements/BaseLayout';

import './normalize.css';
import './global.css';
import './headings.css';

const App = () => (
  <BaseLayout>
    <TestPage />
  </BaseLayout>
);

export default App;
