/* eslint-disable no-console */
const next = require('next');
const express = require('express');
const enforce = require('express-sslify');
const { createProxyMiddleware } = require('http-proxy-middleware');
const { initAuth0 } = require('@auth0/nextjs-auth0');
const config = require('./next.config');

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const app = next({ dev });
const handle = app.getRequestHandler();

let server;
app
  .prepare()
  .then(() => {
    server = express();

    server.use(
      '/backend',
      function(req, res, callback) {
        const auth0 = initAuth0({
          audience: config.env.API_AUDIENCE,
          clientId: config.env.AUTH0_CLIENT_ID,
          clientSecret: config.env.AUTH0_CLIENT_SECRET,
          scope: config.env.AUTH0_SCOPE,
          domain: config.env.AUTH0_DOMAIN,
          redirectUri: config.env.REDIRECT_URI,
          postLogoutRedirectUri: config.env.POST_LOGOUT_REDIRECT_URI,
          session: {
            cookieSecret: config.env.SESSION_COOKIE_SECRET,
            cookieLifetime: config.env.SESSION_COOKIE_LIFETIME,
            storeIdToken: false,
            storeRefreshToken: true,
            storeAccessToken: true,
          },
        });

        auth0
          .getSession(req)
          .then(session => {
            if (!session || !session.user) {
              res
                .status(401)
                .json({ status: 'Unauthorized' })
                .end();

              return Promise.resolve();
            }

            const tokenCache = auth0.tokenCache(req, res);

            return tokenCache.getAccessToken(tokenCache).then(({ accessToken }) => {
              req.headers.Authorization = `Bearer ${accessToken}`;
            });
          })
          .finally(() => callback());
      },
      createProxyMiddleware({
        target: config.env.REST_API_URL,
        changeOrigin: true,
        pathRewrite: { '^/backend': '' },
      }),
    );
    server.use(
      '/download',
      createProxyMiddleware({
        target: 'https://storage.googleapis.com',
        changeOrigin: true,
        pathRewrite: { '^/download': '' },
      }),
    );

    if (process.env.NODE_ENV === 'production') {
      server.use(enforce.HTTPS({ trustProtoHeader: true }));
    }

    server.all('*', (req, res) => handle(req, res));

    server.listen(port, err => {
      if (err) {
        throw err;
      }
      console.log(`> Ready on port ${port}`);
    });
  })
  .catch(err => {
    console.log('An error occurred, unable to start the server');
    console.log(err);
  });
